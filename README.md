# Personal Directory Service
You can visit the [landing page of PDS](personaldirectoryservice.com) to get more information on how the service works, what problems it are trying to solve, some example cases of use and to play around with the API.

## Problem
When designing and building decentralized applications that do not rely on a server one reoccuring problem is how users are to find each others IPs as well as how they are to verify each others identities by knowing other users public keys. This tends to result in two varieties of solutions. A gossip approach that is truly serverless but which is vulernable to islands of users not being able to discover each other. Alternatively a small discovery and authentication server is built and maintained by the creators of the application.

## Purpose
PersonalDirectoryService.com was created in order to provide a general way for other decentralized applications to not have to build and maintain their own discovery/authentication server while avoiding the problems of a truly decentralized gossip approach. PDS is meant to be able to handle many applications/services/users at the same time from entirely different sectors at the same time that PDS knows nothing about these sectors. Users of PDS are meant to be able to get their exact needs met while co-existing with other users of entirely different platforms without any problems. PDS is also meant to be entirely free, as in both beer and freedom, the code is open source and there is absolutely no cost for usage by application developers or by regular users. 

The way to interact with PDS is through a very small HTTP API consisting of 6 endpoints. These include: registering a user, logging in, logging out, searching for a users IP and public key, registering interest in a topic, searching for users interested in a topic. The API is originally intended and designed around robot usage for application builders, but a human API is provided in the API section where you can play around with it. 

Users can choose who is able to find them in by using the filtering system in order to preserve privacy and allow total control by users. The filtering system is described in the design section.

## Very basic overview
The basics of PDS is that it's a service which let's you register an account and then lets you login for other people to find your IP address and your public key. It also allows you to search for other users IP address and public key. It has several features that lets you decide who exactly should be able to find you and when.
The main target audience of this service is peer-to-peer application developers. This service is meant to allow developers to not have to build and maintain their own authentication/discovery server.
