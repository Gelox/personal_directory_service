use crate::database::{get, insert};
use crate::models::{Error, LoginInformation};
use crate::utils::get_username_from_token;
use crate::TOPIC_DELIMITER;
use chrono::Utc;
use serde::Deserialize;
use sled::Tree;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct RegisterTopicQuery {
    pub topic: String,
    pub token: String,
}

pub async fn register_topic(
    par: RegisterTopicQuery,
    login_tree: Tree,
    topic_tree: Tree,
) -> Result<impl warp::Reply, warp::Rejection> {
    if par.topic.contains(TOPIC_DELIMITER) {
        return Err(Error::Unspecified(format!(
            "Topic names can not contain {}",
            TOPIC_DELIMITER
        )))?;
    }
    let username = get_username_from_token(&par.token)?;
    let login: LoginInformation = get(login_tree, username).await?;
    // Authenticate
    if login.token != par.token || login.expires <= Utc::now() {
        return Err(Error::user_not_found())?;
    }
    // NOTE: We prefix the username with the topic and a unique TOPIC_DELIMITER in order to be able
    // to efficiently search the DB later
    let prefixed_username = format!("{}{}{}", par.topic, TOPIC_DELIMITER, login.username);
    // NOTE: We need to register the token as the value in order to be able to see if the login has
    // expired since registration.
    insert(topic_tree, prefixed_username, login.token).await?;
    Ok(warp::reply::json(&""))
}
