use crate::database::{insert_strict, DatabaseError};
use crate::models::PubKey;
use crate::models::{Error, User};
use crate::utils::hash;
use crate::TOPIC_DELIMITER;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct RegisterRequest {
    pub username: String,
    #[serde(default)]
    pub pub_key: Option<PubKey>,
    pub password: String,
}

pub async fn register(
    par: RegisterRequest,
    user_tree: sled::Tree,
) -> Result<impl warp::Reply, warp::Rejection> {
    if par.username.contains(TOPIC_DELIMITER) {
        // In order to prevent usernames to be used as an attack on the topic register we can't
        // allow them to contain the delimiter
        return Err(Error::warp_err(Error::InvalidArgument(format!(
            "Username can not contain {}",
            TOPIC_DELIMITER
        ))));
    }

    if par.username == String::new() {
        return Err(Error::warp_err(Error::InvalidArgument(format!(
            "Username can not be an empty string",
        ))));
    }

    if par.password == String::new() {
        return Err(Error::warp_err(Error::InvalidArgument(format!(
            "Password can not be an empty string",
        ))));
    }

    let user = User {
        username: par.username,
        pub_key: par.pub_key,
        hashed_password: hash(&par.password.as_bytes()),
    };

    match insert_strict(user_tree, user.username.clone(), user).await {
        Ok(()) => (),
        Err(DatabaseError::InsertFailedKeyExists) => {
            return Err(warp::reject::custom(Error::username_already_registered()))
        }
        Err(_) => {
            return Err(warp::reject::custom(Error::DatabaseError(format!(
                "Could not insert"
            ))))
        }
    };

    Ok(warp::reply::json(&""))
}
