use crate::database::{get, remove};
use crate::models::{Error, LoginInformation, User};
use crate::utils::verify_hash;
use serde::Deserialize;
use sled::Tree;

#[derive(Deserialize, Debug, Clone, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
pub enum LogoutAuthentication {
    Password(String),
    Token(String),
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct LogoutQuery {
    pub username: String,
    #[serde(flatten)]
    pub auth: LogoutAuthentication,
}

pub async fn logout(
    par: LogoutQuery,
    user_tree: Tree,
    login_tree: Tree,
) -> Result<impl warp::Reply, warp::Rejection> {
    // TODO: Make all authentication fail identically, also when the user does not exist
    // If we fail authentication then we say that the user is not found
    match par.auth {
        LogoutAuthentication::Password(pass) => {
            let user: User = get(user_tree, par.username.clone())
                .await
                .map_err(|_| Error::failed_authentication())?;
            if !verify_hash(&user.hashed_password, &pass.as_bytes()) {
                return Err(Error::failed_authentication())?;
            }
        }
        LogoutAuthentication::Token(token) => {
            let login_info: LoginInformation = get(login_tree.clone(), par.username.clone())
                .await
                .map_err(|_| Error::failed_authentication())?;
            if login_info.token != token {
                return Err(Error::failed_authentication())?;
            }
        }
    };
    remove(login_tree, par.username).await?;
    Ok(warp::reply::json(&""))
}
