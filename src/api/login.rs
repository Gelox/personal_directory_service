use crate::database::{get, insert};
use crate::models::{Error, LoginInformation, User};
use crate::utils::{new_token, verify_hash};
use chrono::Utc;
use log::*;
use serde::Deserialize;
use std::net::{IpAddr, SocketAddr};

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct LoginQuery {
    pub username: String,
    #[serde(default)]
    pub ttl: Option<i64>,
    pub password: String,
    #[serde(default)]
    pub keyword_filters: Option<Vec<String>>,
    #[serde(default)]
    pub username_filters: Option<Vec<String>>,
}

pub async fn login(
    par: LoginQuery,
    ip: Option<SocketAddr>,
    forwarded_ip: Option<IpAddr>,
    user_tree: sled::Tree,
    login_tree: sled::Tree,
) -> Result<impl warp::Reply, warp::Rejection> {
    // If no forwarded IP is provided then the real IP is used
    let ip = match forwarded_ip {
        Some(ip) => ip,
        None => match ip {
            Some(ip) => ip.ip(),
            None => return Err(Error::could_not_get_ip())?,
        },
    };

    let user: User = get(user_tree, par.username).await.map_err(|e| {
        error!("{:?}", &e);
        Error::from(e)
    })?;
    // Authenticate password here
    if !verify_hash(&user.hashed_password, &par.password.as_bytes()) {
        return Err(Error::failed_authentication())?;
    }

    let token = new_token(user.username.clone())?;
    // NOTE: Magic number is 60 seconds times 20 minutes
    let expires = Utc::now() + chrono::Duration::seconds(par.ttl.unwrap_or(60 * 20));
    let login_entry = LoginInformation {
        username: user.username,
        expires,
        ip,
        keyword_filters: par.keyword_filters.clone(),
        username_filters: par.username_filters.clone(),
        token: token.clone(),
    };
    insert(login_tree, login_entry.username.clone(), login_entry)
        .await
        .map_err(|e| Error::from(e))?;

    Ok(warp::reply::json(&token))
}
