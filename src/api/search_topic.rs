use crate::database::{get, get_all_kv, remove, DatabaseError};
use crate::models::{Error, LoginInformation, SearchResult, User};
use crate::utils::{get_username_from_token, passes_filter};
use crate::TOPIC_DELIMITER;
use chrono::Utc;
use futures::future::join_all;
use log::*;
use serde::{Deserialize, Serialize};
use sled::Tree;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SearchTopicQuery {
    pub topic: String,
    #[serde(default)]
    pub keyword_filters: Option<Vec<String>>,
    #[serde(default)]
    pub token: Option<String>,
    #[serde(default)]
    pub cursor: Option<String>,
    #[serde(default)]
    pub offset: Option<usize>,
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SearchTopicResponse {
    pub users: Vec<SearchResult>,
    pub cursor: Option<String>,
}

pub async fn search_topic(
    par: SearchTopicQuery,
    user_tree: Tree,
    login_tree: Tree,
    topic_tree: Tree,
) -> Result<impl warp::Reply, warp::Rejection> {
    // TODO: Currently we use the cursor as a number, in the future we might use it as a string, which is
    // why the public interface is a string. A string would make the cursor be able to specify a
    // specific key in the database much more easily.
    let cursor = par
        .cursor
        .as_ref()
        .unwrap_or(&format!("0"))
        .parse::<usize>()
        .map_err(|_| Error::Unspecified(format!("Could not parse cursor")))?;
    let offset = par.offset.unwrap_or(50);
    // We need to parse the query separately since warp does not parse querys in the way we would
    // like to
    //let par = parse_url_query_parameters::<SearchTopicQuery>(par)?;
    info!(target: "HTTP events", "Search topic");
    // If the searcher provided a token we can look up their username to give them additional
    // privileges
    let searcher_username = match &par.token {
        Some(token) => {
            let searcher_username = get_username_from_token(token)?;
            let login: LoginInformation = get(login_tree.clone(), searcher_username).await?;
            if &login.token != token {
                return Err(Error::failed_authentication())?;
            }
            Some(login.username)
        }
        None => None,
    };
    // We provide a prefix to the search using the topic and the topic delimiter
    let registered: Vec<(String, String)> = get_all_kv(
        topic_tree.clone(),
        format!("{}{}", par.topic, TOPIC_DELIMITER),
        offset,
    )
    .await
    .map_err(|e| Error::from(e))?;
    // Get current time to check expired logins
    let now = Utc::now();
    // This massive map creates a future for each of the registered users and performs the logic to
    // 1) see if they are logged in 2) see that their session is still the one they registered
    //    interest with 3) see that the searcher is allowed to get past their filters
    let futs: Vec<_> = registered
        .into_iter()
        .map(|(username, token)| {
            let par = &par;
            let login_tree = login_tree.clone();
            let topic_tree = topic_tree.clone();
            let searcher_username = &searcher_username;
            async move {
                let user: LoginInformation = match get(login_tree.clone(), username).await {
                    Ok(user) => user,
                    Err(DatabaseError::NotFound) => return Ok(None),
                    Err(e) => return Err(Error::from(e)),
                };
                if user.token != token {
                    // If the logged in user has a different token than what is used in registration then
                    // the registration is no longer valid
                    if let Err(e) = remove(
                        topic_tree,
                        format!("{}{}{}", par.topic, TOPIC_DELIMITER, user.username),
                    )
                    .await
                    {
                        error!("Could not remove expired topic registration due to {:?}", e)
                    };
                    return Ok(None);
                }
                if user.expires <= now {
                    // Remove expired logins
                    tokio::join!(
                        async {
                            if let Err(e) = remove(login_tree, user.username.clone()).await {
                                error!("Could not remove expired topic registration due to {:?}", e)
                            }
                        },
                        async {
                            if let Err(e) = remove(
                                topic_tree,
                                format!(
                                    "{}{}{}",
                                    par.topic,
                                    TOPIC_DELIMITER,
                                    user.username.clone()
                                ),
                            )
                            .await
                            {
                                error!("Could not remove expired topic registration due to {:?}", e)
                            }
                        }
                    );
                    return Ok(None);
                } else if passes_filter(
                    &par.keyword_filters,
                    &user.keyword_filters,
                    &user.username_filters,
                    &searcher_username,
                ) {
                    return Ok(Some(user));
                } else {
                    return Ok(None);
                }
            }
        })
        .collect();

    let mut returned_cursor = None;

    let user_results = join_all(futs).await;
    let mut users = Vec::with_capacity(user_results.len());
    // Skip the first `start` users and return at maximum `limit` users
    for user_r in user_results.into_iter().skip(cursor) {
        // If the limit has been reached
        if users.len() >= offset {
            returned_cursor = Some((cursor + offset).to_string());
            break;
        }
        let user_opt = user_r?;
        match user_opt {
            Some(user) => users.push(user),
            None => (),
        }
    }

    // Get each of the users that are to be returned from the database and compile their
    // information
    let users: Vec<_> = users
        .into_iter()
        .map(|user| {
            let user_tree = user_tree.clone();
            async move {
                let pub_key: User = get(user_tree, user.username.clone()).await?;
                let pub_key = pub_key.pub_key;
                Result::<_, Error>::Ok(SearchResult {
                    username: user.username,
                    ip: user.ip,
                    pub_key,
                })
            }
        })
        .collect();

    let users = join_all(users).await;
    let mut results = Vec::with_capacity(users.len());
    for user in users {
        results.push(user?);
    }
    Ok(warp::reply::json(&SearchTopicResponse {
        users: results,
        cursor: returned_cursor,
    }))
}
