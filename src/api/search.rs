use crate::database::{get, remove};
use crate::models::{Error, LoginInformation, SearchResult, User};
use crate::utils::{get_username_from_token, passes_filter};
use chrono::Utc;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SearchQuery {
    pub username: String,
    pub keyword_filters: Option<Vec<String>>,
    pub token: Option<String>,
}

pub async fn search(
    par: SearchQuery,
    user_tree: sled::Tree,
    login_tree: sled::Tree,
) -> Result<impl warp::Reply, warp::Rejection> {
    //Authenticate
    // TODO: Make everything return the same thing if authentication fails
    let login: LoginInformation = get(login_tree.clone(), par.username).await?;
    let searcher_username = match &par.token {
        Some(token) => Some(get_username_from_token(token)?),
        None => None,
    };
    let now = Utc::now();
    if login.expires <= now {
        remove(login_tree, login.username).await?;
        return Err(Error::user_not_found())?;
    } else if !passes_filter(
        &par.keyword_filters,
        &login.keyword_filters,
        &login.username_filters,
        &searcher_username,
    ) {
        return Err(Error::user_not_found())?;
    }

    let user: User = get(user_tree, login.username).await?;
    Ok(warp::reply::json(&SearchResult {
        username: user.username,
        ip: login.ip,
        pub_key: user.pub_key,
    }))
}
