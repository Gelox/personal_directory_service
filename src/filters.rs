use crate::api::*;
use crate::handle_rejection::handle_rejection;
use crate::{LOGIN_TREE, TOPIC_TREE, USER_TREE};
use std::net::IpAddr;
use warp::http::Method;
use warp::Filter;

const MAX_BODY_LIMIT: u64 = 1024 * 16;

#[cfg(debug_assertions)]
macro_rules! maybe_box {
    ($expression:expr) => {
        $expression.boxed()
    };
}

#[cfg(not(debug_assertions))]
macro_rules! maybe_box {
    ($expression:expr) => {
        $expression
    };
}

pub fn filters() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    let cors = warp::cors()
        .allow_any_origin()
        .allow_methods(&[Method::GET, Method::POST, Method::PUT, Method::DELETE])
        .allow_headers(vec![
            "User-Agent",
            "Authorization",
            "Referer",
            "Origin",
            "Access-Control-Request-Method",
            "Access-Control-Request-Headers",
            "Accept",
            "Range",
            "If-Range",
            "Content-Type",
            "Content-Length",
        ])
        .max_age(600);
    // Register
    // POST /register => 201 CREATED
    let register = maybe_box!(warp::path!("register")
        .and(warp::path::end())
        .and(warp::post())
        .and(warp::body::content_length_limit(MAX_BODY_LIMIT).and(warp::body::json()))
        .and_then(|par| async { register(par, USER_TREE.clone()).await }));

    // Login
    // POST /login => 200 OK JSON(token)
    let login = maybe_box!(warp::path!("login")
        .and(warp::post())
        .and(warp::body::content_length_limit(MAX_BODY_LIMIT).and(warp::body::json()))
        .and(warp::addr::remote())
        // Take the forwarded IP from nginx
        .and(warp::header::optional::<IpAddr>("X-Real-IP"))
        .and_then(|par, ip, forwarded_ip| async move {
            login(par, ip, forwarded_ip, USER_TREE.clone(), LOGIN_TREE.clone()).await
        },));

    // Search
    // POST /search?username=(username)&filter=({filter, }) => 200 OK JSON(IP, pub_key)
    let search = maybe_box!(warp::path!("search")
        .and(warp::post())
        //.and(warp::query::<SearchQueryParams>())
        .and(warp::body::content_length_limit(MAX_BODY_LIMIT).and(warp::body::json()))
        //.and(warp::filters::query::raw())
        .and_then(|par| async { search(par, USER_TREE.clone(), LOGIN_TREE.clone()).await }));

    // Register topic interest
    // PUT /register_topic => 200 OK
    let register_topic = maybe_box!(warp::path!("register_topic")
        .and(warp::put())
        .and(warp::body::content_length_limit(MAX_BODY_LIMIT).and(warp::body::json()))
        .and_then(|par| async {
            register_topic(par, LOGIN_TREE.clone(), TOPIC_TREE.clone()).await
        }));

    // Search topic
    // POST /topic?topic=(topic) => 200 OK JSON(Vec<(username, IP, pub_key)>
    let topic_search = maybe_box!(warp::path!("topic")
        .and(warp::post())
        .and(warp::body::content_length_limit(MAX_BODY_LIMIT).and(warp::body::json()))
        .and_then(|par| async {
            search_topic(
                par,
                USER_TREE.clone(),
                LOGIN_TREE.clone(),
                TOPIC_TREE.clone(),
            )
            .await
        }));

    // Logout
    // PUT /logout => 200 OK
    let logout = maybe_box!(warp::path!("logout")
        .and(warp::put())
        .and(warp::body::content_length_limit(MAX_BODY_LIMIT).and(warp::body::json()))
        .and_then(|par| async { logout(par, USER_TREE.clone(), LOGIN_TREE.clone()).await }));

    // TODO: Could add a unregister from topic endpoint
    let options = warp::any().and(warp::options()).map(|| warp::reply());
    let routes = maybe_box!(search
        .or(login)
        .or(register)
        .or(register_topic)
        .or(topic_search)
        .or(logout)
        .or(options)
        .recover(handle_rejection)
        .with(&cors));
    routes
}

#[cfg(test)]
mod http_tests {
    use super::*;
    use crate::models::*;
    use crate::tests::*;
    use std::net::{IpAddr, Ipv4Addr, SocketAddr};
    use warp::http::StatusCode;
    use warp::test::request;

    async fn register_first_user() {
        register_user(
            TEST_FIRST_USER,
            TEST_FIRST_USER_PASSWORD,
            Some(TEST_FIRST_USER_PUBLIC_KEY),
        )
        .await;
    }

    async fn register_user(username: &str, password: &str, dsasha256key: Option<&str>) {
        let api = filters();
        let pub_key = dsasha256key
            .map(|dsasha256key| {
                format!(
                    r#"",pub_key":{{kind"{}",pub_key:"{}"}},"#,
                    "DsaSha256", dsasha256key
                )
            })
            .unwrap_or(String::new());
        let _resp = request()
            .method("POST")
            .path("/register")
            .body(&format!(
                r#"{{"username":"{}","password":"{}"{}}}"#,
                username, password, pub_key
            ))
            .reply(&api)
            .await;
    }

    //async fn register_users() {
    //    register_user(
    //        TEST_FIRST_USER,
    //        TEST_FIRST_USER_PASSWORD,
    //        Some(TEST_FIRST_USER_PUBLIC_KEY),
    //    )
    //    .await;
    //    register_user(
    //        TEST_SECOND_USER,
    //        TEST_SECOND_USER_PASSWORD,
    //        Some(TEST_FIRST_USER_PUBLIC_KEY),
    //    )
    //    .await;
    //}

    async fn register_and_login_first_user() -> String {
        register_and_login_user(
            TEST_FIRST_USER,
            TEST_FIRST_USER_PASSWORD,
            Some(TEST_FIRST_USER_PUBLIC_KEY),
        )
        .await
    }

    async fn register_and_login_user(
        username: &str,
        password: &str,
        dsasha256key: Option<&str>,
    ) -> String {
        register_and_login_user_with_filter(username, password, dsasha256key, None).await
    }

    async fn register_and_login_first_user_with_filter() -> String {
        register_and_login_user_with_filter(
            TEST_FIRST_USER,
            TEST_FIRST_USER_PASSWORD,
            Some(TEST_FIRST_USER_PUBLIC_KEY),
            Some(vec![TEST_FIRST_FILTER]),
        )
        .await
    }

    async fn register_and_login_user_with_filter(
        username: &str,
        password: &str,
        pubkey: Option<&str>,
        keyword_filters: Option<Vec<&str>>,
    ) -> String {
        register_user(username, password, pubkey).await;
        let api = filters();
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(8, 8, 8, 8)), 8080);
        let keyword_filters = keyword_filters
            .map(|keyword_filters| format!(r#","keywordFilters":{:?}"#, keyword_filters))
            .unwrap_or(String::new());
        let body = format!(
            r#"{{"username":"{}","ttl":{},"password":"{}"{}}}"#,
            username, 600, password, keyword_filters
        );
        let resp = request()
            .method("POST")
            .path("/login")
            .remote_addr(addr)
            .body(body);
        let resp = resp.reply(&api).await;

        assert_eq!(resp.status(), StatusCode::OK);
        let text = std::str::from_utf8(resp.body()).unwrap();
        let token: String = serde_json::from_str(&text).unwrap();
        token
    }

    async fn register_and_login_first_user_and_register_topic() -> String {
        register_and_login_user_and_register_topic(
            TEST_FIRST_USER,
            TEST_FIRST_USER_PASSWORD,
            Some(TEST_FIRST_USER_PUBLIC_KEY),
            Some(vec![TEST_FIRST_FILTER]),
            TEST_FIRST_TOPIC,
        )
        .await
    }

    async fn register_and_login_user_and_register_topic(
        username: &str,
        password: &str,
        pubkey: Option<&str>,
        keyword_filters: Option<Vec<&str>>,
        topic: &str,
    ) -> String {
        let token =
            register_and_login_user_with_filter(username, password, pubkey, keyword_filters).await;
        let api = filters();

        let _resp = request()
            .method("PUT")
            .path("/register_topic")
            .body(format!(r#"{{"token"="{}","topic":"{}"}}"#, token, topic))
            .reply(&api)
            .await;
        token
    }

    #[tokio::test]
    async fn register() {
        let api = filters();

        let body = format!(
            r#"{{"username":"foo","pub_key":{{"kind":"DsaSha256","key":"{}"}},"password":"my_pass"}}"#,
            TEST_FIRST_USER_PUBLIC_KEY
        );
        let resp = request()
            .method("POST")
            .path("/register")
            .body(body)
            .reply(&api)
            .await;

        assert_eq!(resp.status(), StatusCode::CREATED);
        assert_eq!(resp.body(), "");
    }

    #[tokio::test]
    async fn search() {
        register_and_login_first_user().await;
        let api = filters();

        let resp = request()
            .method("GET")
            .path("/search")
            .body(format!(r#"{{"username"="{}"}}"#, TEST_FIRST_USER))
            .reply(&api)
            .await;

        assert_eq!(resp.status(), StatusCode::OK);
        let body = std::str::from_utf8(resp.body()).unwrap();
        let body: SearchResult = serde_json::from_str(&body).unwrap();
        assert_eq!(body.username, TEST_FIRST_USER);
        assert_eq!(
            body.pub_key,
            Some(PubKey {
                kind: String::from("DsaSha256"),
                key: TEST_FIRST_USER_PUBLIC_KEY.to_string()
            })
        );
    }

    #[tokio::test]
    async fn search_with_filters() {
        register_and_login_first_user_with_filter().await;
        let api = filters();

        let resp = request()
            .method("GET")
            .path("/search")
            .body(format!(
                r#"{{"username"="{}","keywordFilters":["{}"]}}"#,
                TEST_FIRST_USER, TEST_FIRST_FILTER
            ))
            .reply(&api)
            .await;

        assert_eq!(resp.status(), StatusCode::OK);
        let body = std::str::from_utf8(resp.body()).unwrap();
        let body: SearchResult = serde_json::from_str(&body).unwrap();
        assert_eq!(body.username, TEST_FIRST_USER);
        assert_eq!(
            body.pub_key,
            Some(PubKey {
                kind: String::from("DsaSha256"),
                key: TEST_FIRST_USER_PUBLIC_KEY.to_string()
            })
        );
    }

    #[tokio::test]
    async fn search_and_find_nothing_without_filters() {
        register_and_login_first_user_with_filter().await;
        let api = filters();

        let resp = request()
            .method("GET")
            .path("/search")
            .body(format!(
                r#"{{"username"="{}","keywordFilters":["{}"]}}"#,
                TEST_FIRST_USER, TEST_SECOND_FILTER
            ))
            .reply(&api)
            .await;

        assert_eq!(resp.status(), StatusCode::NOT_FOUND);
        let body = std::str::from_utf8(resp.body()).unwrap();
        let body: String = serde_json::from_str(&body).unwrap();
        assert_eq!(body, "");
    }

    #[tokio::test]
    async fn search_with_two_filters() {
        register_and_login_first_user_with_filter().await;
        let api = filters();

        let resp = request()
            .method("GET")
            .path("/search")
            .body(format!(
                r#"{{"username"="{}","keywordFilters":["{}","{}"]}}"#,
                TEST_FIRST_USER, TEST_SECOND_FILTER, TEST_FIRST_FILTER
            ))
            .reply(&api)
            .await;

        assert_eq!(resp.status(), StatusCode::OK);
        let body = std::str::from_utf8(resp.body()).unwrap();
        let body: SearchResult = serde_json::from_str(&body).unwrap();
        assert_eq!(body.username, TEST_FIRST_USER);
        assert_eq!(
            body.pub_key,
            Some(PubKey {
                kind: String::from("DsaSha256"),
                key: TEST_FIRST_USER_PUBLIC_KEY.to_string()
            })
        );
    }

    #[tokio::test]
    async fn login() {
        register_first_user().await;
        let api = filters();
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(8, 8, 8, 8)), 8080);
        let resp = request()
            .method("POST")
            .path("/login")
            .remote_addr(addr)
            .body(format!(
                r#"{{"username"="{}","password":"{}","ttl":600}}"#,
                TEST_FIRST_USER, TEST_FIRST_USER_PASSWORD
            ));
        let resp = resp.reply(&api).await;

        assert_eq!(resp.status(), StatusCode::OK);
        let body = std::str::from_utf8(resp.body()).unwrap();
        let _token: String = serde_json::from_str(&body).unwrap();
    }

    #[tokio::test]
    async fn register_topic() {
        let user_token = register_and_login_first_user().await;
        let api = filters();

        let resp = request()
            .method("PUT")
            .path("/register_topic")
            .body(format!(
                r#"{{"token"="{}","topic":"{}"}}"#,
                user_token, "some_topic"
            ))
            .reply(&api)
            .await;

        assert_eq!(resp.status(), StatusCode::OK);
        assert_eq!(resp.body(), "");
    }

    #[tokio::test]
    async fn search_topic() {
        register_and_login_first_user_and_register_topic().await;
        let api = filters();
        let resp = request()
            .method("GET")
            .path("/topic")
            .body(format!(r#"{{"topic"="{}"}}"#, TEST_FIRST_TOPIC))
            .reply(&api)
            .await;

        assert_eq!(resp.status(), StatusCode::OK);
        let body = std::str::from_utf8(resp.body()).unwrap();
        let body: SearchResult = serde_json::from_str(&body).unwrap();
        assert_eq!(body.username, TEST_FIRST_USER);
        assert_eq!(
            body.pub_key,
            Some(PubKey {
                kind: String::from("DsaSha256"),
                key: TEST_FIRST_USER_PUBLIC_KEY.to_string()
            })
        );
    }

    #[tokio::test]
    async fn logout() {
        let user_token = register_and_login_first_user().await;
        let api = filters();
        let resp = request().method("PUT").path("/logout").body(format!(
            r#"{{"username":"{}","auth":{{"token":"{}"}}}}"#,
            TEST_FIRST_USER, user_token
        ));
        let resp = resp.reply(&api).await;

        assert_eq!(resp.status(), StatusCode::OK);
        //TODO test to make sure body is alright
        let body = std::str::from_utf8(resp.body()).unwrap();
        assert_eq!(body, "");
    }

    //TODO: Fix these tests up
    //#[tokio::test]
    //async fn does_not_find_username_filter_user_without_correct_token() {
    //    register_users().await;
    //    let wrong_token = cont
    //        .clone()
    //        .login(
    //            TEST_FIRST_USER.to_string(),
    //            600,
    //            Authentication::Password(TEST_FIRST_USER_PASSWORD.to_string()),
    //            SocketAddr::new(IpAddr::V4(Ipv4Addr::new(8, 8, 8, 8)), 8080),
    //            Some(vec![TEST_FIRST_FILTER.to_string()]),
    //            Some(vec![TEST_SECOND_USER.to_string()]),
    //        )
    //        .await
    //        .unwrap();
    //    let api = filters(cont);
    //    let resp = request().method("GET").path(&format!(
    //        "/search?username={}&filters[]={}",
    //        TEST_FIRST_USER, TEST_FIRST_FILTER,
    //    ));
    //    let resp = resp.reply(&api).await;

    //    assert_eq!(resp.status(), StatusCode::NOT_FOUND);
    //    assert_eq!(resp.body(), "\"\"");

    //    let resp = request().method("GET").path(&format!(
    //        "/search?username={}&token={}&filters[]={}",
    //        TEST_FIRST_USER, wrong_token, TEST_FIRST_FILTER,
    //    ));
    //    let resp = resp.reply(&api).await;

    //    assert_eq!(resp.status(), StatusCode::NOT_FOUND);
    //    assert_eq!(resp.body(), "\"\"");
    //}

    //#[tokio::test]
    //async fn search_user_with_login_filter() {
    //    let service_name = "Test service";
    //    let db = InMemoryDB::new();
    //    let cont = Controller::new(db, service_name.to_string());
    //    register_users(cont.clone()).await;
    //    cont.clone()
    //        .login(
    //            TEST_FIRST_USER.to_string(),
    //            600,
    //            Authentication::Password(TEST_FIRST_USER_PASSWORD.to_string()),
    //            test_ip(),
    //            Some(vec![TEST_FIRST_FILTER.to_string()]),
    //            Some(vec![TEST_SECOND_USER.to_string()]),
    //        )
    //        .await
    //        .unwrap();
    //    let second_token = cont
    //        .clone()
    //        .login(
    //            TEST_SECOND_USER.to_string(),
    //            600,
    //            Authentication::Password(TEST_SECOND_USER_PASSWORD.to_string()),
    //            test_ip(),
    //            None,
    //            None,
    //        )
    //        .await
    //        .unwrap();
    //    let api = filters(cont);
    //    let resp = request().method("GET").path(&format!(
    //        "/search?username={}&token={}&filters[]={}",
    //        TEST_FIRST_USER, second_token, TEST_FIRST_FILTER,
    //    ));
    //    let resp = resp.reply(&api).await;

    //    assert_eq!(resp.status(), StatusCode::OK);
    //    assert_eq!(resp.body(), "{\"username\":\"user_a\",\"ip\":\"127.0.0.1:8080\",\"pub_key\":{\"kind\":\"DsaSha256\",\"key\":\"-----BEGIN PUBLIC KEY-----\\nMIIDRjCCAjkGByqGSM44BAEwggIsAoIBAQCtzEMWBVlcGjkbjMd1JZ7iGBEtTFoa\\ngphB63qLW6GoUB5I6aeVDe1X7aw2+bbcpEbvPrfgvEtC6C4fdS4rWQupV5XRkfcp\\nfDB+coEubeZVDHZg4VU5vkfgDuRaq0ZkK9NhEG0YN8r0cwomCxTdiG53tdkVPl0r\\nxyQyeqVXRrucSJQIJJDw0qc1+nWxLSrV7ZYKg0f+kSESYb0CCn/tvpmhPrQtkHRJ\\nhubTloci9IwsXE2KDLtm6NDqj/9JM9unqxh+XZr4oIHZvKv+xhG03D9ky+9yxFFu\\nRXj759TVcvKCxXsooiF4WHRQGovW2zgVCzUBsOOkjBSDygGK+ZqdDhPBAiEAtPJI\\njfXolYzAmkJfLOU+Xqrq7EbAqcCtSA5nxfPnQd0CggEAQnDildyWvknyiX1ck8yW\\nmzxr8qEIBdPhOM3oqU3DVr+Yu/cV7pvXBt3k/algXf+DAST5CBMX/GYlWvJfMVjf\\nLFaGFGzZV9FmmgT8KLCi6FRp7vv9zfKQZp+zldToFPY/LnzIXF7EFfJeS2v61xSd\\nPdAGpb5ZqNP4uNr/NUByKj1gAjjorSr/wKDRVGq0hxvL6V7mtBeXYO2iP6NShRxK\\nQ2jrd9kv3zwZvX/Sz/X3NKS9SY6Do0/AsXCj/TmfvrzqBdCiODloxjv4U1c35VT7\\n408jNGZrL3p48kfWSEPxrQEkxdNlHAnM819rHxoK1VqciV8qfMpQts4xTev5Cbtu\\nlQOCAQUAAoIBAAiAPoGftnELsNol2/WhBchj3YY9C/2FWzhwQkeVU0K5ziHwVZtu\\nQUWTg4E+t48I36Rg+0+UE+WpyBm04WsBztXmV/Tc64kVf+/AC1qMHFhN21J81L2+\\nqnkL5pumSZf8klgM73kI6/LuwWaaiuChGHizwTgZBlA0QLpLiaFzInn/x8rBfpvv\\nJeCTNaPOI3EaP1g731IBo9RWDBQwa5BiuYt+3BXH4zAJgzgqxB3W44F8PKzqvmJ0\\njzYVoClBhr/juT9TJSqK1YejpixlYrW1HheKn+RSHU7k46qXk+8vpIZuT16vApSW\\n7DlfsFaJHpB+esPAtqNCLVy9EzDe3IO0dQw=\\n-----END PUBLIC KEY-----\\n\"}}");
    //}

    //// TODO test for registered topic with login filter
    //#[tokio::test]
    //async fn search_topic_with_login_filter() {
    //    let service_name = "Test service";
    //    let db = InMemoryDB::new();
    //    let cont = Controller::new(db, service_name.to_string());
    //    register_users(cont.clone()).await;
    //    let wrong_token = cont
    //        .clone()
    //        .login(
    //            TEST_FIRST_USER.to_string(),
    //            600,
    //            Authentication::Password(TEST_FIRST_USER_PASSWORD.to_string()),
    //            test_ip(),
    //            Some(vec![TEST_FIRST_FILTER.to_string()]),
    //            Some(vec![TEST_SECOND_USER.to_string()]),
    //        )
    //        .await
    //        .unwrap();
    //    let second_token = cont
    //        .clone()
    //        .login(
    //            TEST_SECOND_USER.to_string(),
    //            600,
    //            Authentication::Password(TEST_SECOND_USER_PASSWORD.to_string()),
    //            test_ip(),
    //            None,
    //            None,
    //        )
    //        .await
    //        .unwrap();
    //    cont.clone()
    //        .register_topic(wrong_token.clone(), TEST_FIRST_TOPIC.to_string())
    //        .await
    //        .unwrap();

    //    let api = filters(cont);
    //    let resp = request().method("GET").path(&format!(
    //        "/topic?topic={}&filters[]={}",
    //        TEST_FIRST_TOPIC, TEST_FIRST_FILTER,
    //    ));
    //    let resp = resp.reply(&api).await;

    //    assert_eq!(resp.status(), StatusCode::OK);
    //    assert_eq!(resp.body(), "[]");

    //    let resp = request().method("GET").path(&format!(
    //        "/topic?topic={}&token={}&filters[]={}",
    //        TEST_FIRST_TOPIC, wrong_token, TEST_FIRST_FILTER,
    //    ));
    //    let resp = resp.reply(&api).await;

    //    assert_eq!(resp.status(), StatusCode::OK);
    //    assert_eq!(resp.body(), "[]");

    //    let resp = request().method("GET").path(&format!(
    //        "/topic?topic={}&token={}&filters[]={}",
    //        TEST_FIRST_TOPIC, second_token, TEST_SECOND_FILTER,
    //    ));
    //    let resp = resp.reply(&api).await;

    //    assert_eq!(resp.status(), StatusCode::OK);
    //    assert_eq!(resp.body(), "[]");

    //    let resp = request().method("GET").path(&format!(
    //        "/topic?topic={}&token={}&filters[]={}",
    //        TEST_FIRST_TOPIC, second_token, TEST_FIRST_FILTER,
    //    ));
    //    let resp = resp.reply(&api).await;

    //    assert_eq!(resp.status(), StatusCode::OK);
    //    assert_eq!(resp.body(), "[{\"username\":\"user_a\",\"ip\":\"127.0.0.1:8080\",\"pub_key\":{\"kind\":\"DsaSha256\",\"key\":\"-----BEGIN PUBLIC KEY-----\\nMIIDRjCCAjkGByqGSM44BAEwggIsAoIBAQCtzEMWBVlcGjkbjMd1JZ7iGBEtTFoa\\ngphB63qLW6GoUB5I6aeVDe1X7aw2+bbcpEbvPrfgvEtC6C4fdS4rWQupV5XRkfcp\\nfDB+coEubeZVDHZg4VU5vkfgDuRaq0ZkK9NhEG0YN8r0cwomCxTdiG53tdkVPl0r\\nxyQyeqVXRrucSJQIJJDw0qc1+nWxLSrV7ZYKg0f+kSESYb0CCn/tvpmhPrQtkHRJ\\nhubTloci9IwsXE2KDLtm6NDqj/9JM9unqxh+XZr4oIHZvKv+xhG03D9ky+9yxFFu\\nRXj759TVcvKCxXsooiF4WHRQGovW2zgVCzUBsOOkjBSDygGK+ZqdDhPBAiEAtPJI\\njfXolYzAmkJfLOU+Xqrq7EbAqcCtSA5nxfPnQd0CggEAQnDildyWvknyiX1ck8yW\\nmzxr8qEIBdPhOM3oqU3DVr+Yu/cV7pvXBt3k/algXf+DAST5CBMX/GYlWvJfMVjf\\nLFaGFGzZV9FmmgT8KLCi6FRp7vv9zfKQZp+zldToFPY/LnzIXF7EFfJeS2v61xSd\\nPdAGpb5ZqNP4uNr/NUByKj1gAjjorSr/wKDRVGq0hxvL6V7mtBeXYO2iP6NShRxK\\nQ2jrd9kv3zwZvX/Sz/X3NKS9SY6Do0/AsXCj/TmfvrzqBdCiODloxjv4U1c35VT7\\n408jNGZrL3p48kfWSEPxrQEkxdNlHAnM819rHxoK1VqciV8qfMpQts4xTev5Cbtu\\nlQOCAQUAAoIBAAiAPoGftnELsNol2/WhBchj3YY9C/2FWzhwQkeVU0K5ziHwVZtu\\nQUWTg4E+t48I36Rg+0+UE+WpyBm04WsBztXmV/Tc64kVf+/AC1qMHFhN21J81L2+\\nqnkL5pumSZf8klgM73kI6/LuwWaaiuChGHizwTgZBlA0QLpLiaFzInn/x8rBfpvv\\nJeCTNaPOI3EaP1g731IBo9RWDBQwa5BiuYt+3BXH4zAJgzgqxB3W44F8PKzqvmJ0\\njzYVoClBhr/juT9TJSqK1YejpixlYrW1HheKn+RSHU7k46qXk+8vpIZuT16vApSW\\n7DlfsFaJHpB+esPAtqNCLVy9EzDe3IO0dQw=\\n-----END PUBLIC KEY-----\\n\"}}]");
    //}
}
