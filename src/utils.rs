use crate::models::Error;
use crate::Result;
use log::{Level, LevelFilter, Metadata, Record, SetLoggerError};
use rand::{Rng, RngCore};

const TOKEN_LEN: usize = 256;

/// Takes a cryptographic-RNG
pub fn new_token(username: String) -> Result<String> {
    let mut rng = rand::thread_rng();
    let mut array = [0; TOKEN_LEN];
    match rng.try_fill_bytes(&mut array) {
        Ok(()) => (),
        Err(_) => return Err(Error::could_not_generate_token()),
    }
    let mut array = array.to_vec();
    array.append(&mut username.into_bytes());
    let token = openssl::base64::encode_block(&array);
    Ok(token)
}

pub fn get_username_from_token(token: &String) -> Result<String> {
    let token = openssl::base64::decode_block(token).map_err(|_| Error::corrupt_token())?;
    if token.len() < TOKEN_LEN {
        return Err(Error::corrupt_token());
    }
    let username =
        String::from_utf8(token[TOKEN_LEN..].to_vec()).map_err(|_| Error::corrupt_token())?;
    Ok(username)
}

pub fn hash(text: &[u8]) -> String {
    let salt = rand::thread_rng().gen::<[u8; 32]>();
    let config = argon2::Config::default();
    argon2::hash_encoded(text, &salt, &config).unwrap()
}

// Verify hash using Argon2.
pub fn verify_hash(hash: &str, password: &[u8]) -> bool {
    argon2::verify_encoded(hash, password).unwrap_or(false)
}

static LOGGER: Logger = Logger;

pub fn init() -> std::result::Result<(), SetLoggerError> {
    log::set_logger(&LOGGER).map(|()| log::set_max_level(LevelFilter::Info))
}

struct Logger;

impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Info
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!("{} - {}", record.level(), record.args());
        }
    }

    fn flush(&self) {}
}

/// The username filter is prioritized over the keyword filter and as such if a user has specified
/// a username filter then the keyword filter is completely ignored.
/// If a searcher exists in the username filter list then they pass the filtering.
/// If a searcher does not then they fail.
/// If a username list is not provided by a user then the keyword list functions the same,
/// if a searcher provides any keyword that exists in the list then they pass the filter, if they
/// do not then they fail.
/// If the user does not provide any type of filter then the searcher is always accepted.
pub fn passes_filter(
    searcher_keyword_filters: &Option<Vec<String>>,
    user_keyword_filters: &Option<Vec<String>>,
    user_username_filters: &Option<Vec<String>>,
    searcher_username: &Option<String>,
) -> bool {
    if let Some(user_username_filters) = user_username_filters {
        match searcher_username {
            Some(searcher_username) => {
                if user_username_filters.contains(searcher_username) {
                    return true;
                } else {
                    return false;
                }
            }
            None => {
                return false;
            }
        }
    }
    if let Some(user_keyword_filters) = user_keyword_filters {
        if let Some(searcher_keyword_filters) = searcher_keyword_filters {
            for searched_keyword in searcher_keyword_filters {
                if user_keyword_filters.contains(searched_keyword) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    true
}
