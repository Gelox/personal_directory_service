use crate::models::Error;
use rayon::prelude::*;
use serde::{de::DeserializeOwned, Serialize};
use sled::Tree;
use tokio::task::spawn_blocking;

#[derive(Debug, Clone)]
pub enum DatabaseError {
    InsertFailedKeyExists,
    CouldNotOpenDatabase(String),
    TableMissing,
    ReadOnlyDatabase,
    SerializationError(String),
    NotFound,
    ModificationError(Error),
    ConcurrencyError,
    SledError(sled::Error),
}

impl From<sled::Error> for DatabaseError {
    fn from(e: sled::Error) -> Self {
        match e {
            sled::Error::CollectionNotFound(_) => Self::NotFound,
            sled::Error::Corruption { .. } => Self::SledError(e),
            sled::Error::Io(_) => Self::SledError(e),
            sled::Error::ReportableBug(_) => Self::SledError(e),
            sled::Error::Unsupported(_) => Self::SledError(e),
        }
    }
}

impl From<DatabaseError> for warp::Rejection {
    fn from(e: DatabaseError) -> Self {
        warp::Rejection::from(crate::models::Error::from(e))
    }
}

impl From<DatabaseError> for crate::models::Error {
    fn from(e: DatabaseError) -> Self {
        match e {
            DatabaseError::InsertFailedKeyExists => {
                Self::DatabaseError(format!("Insert failed, key already exists"))
            }
            DatabaseError::CouldNotOpenDatabase(e) => Self::DatabaseError(e),
            DatabaseError::TableMissing => Self::DatabaseError(format!("Table is missing")),
            DatabaseError::ReadOnlyDatabase => Self::DatabaseError(format!("Read only database")),
            DatabaseError::SerializationError(s) => Self::SerializationError(s),
            DatabaseError::NotFound => Self::NotFound(String::new()),
            DatabaseError::SledError(e) => Self::DatabaseError(format!("Sled error: {}", e)),
            DatabaseError::ConcurrencyError => Self::DatabaseError(format!("Concurrency error")),
            DatabaseError::ModificationError(e) => e,
        }
    }
}

pub async fn get_all_kv<D: DeserializeOwned + std::fmt::Debug + Send + Sync + 'static>(
    tree: Tree,
    prefix: String,
    limit: usize,
) -> std::result::Result<Vec<(String, D)>, DatabaseError> {
    spawn_blocking(move || {
        let objects = tree
            .scan_prefix(&prefix)
            .par_bridge()
            .map::<_, Result<_, DatabaseError>>(|r| {
                let (k, v) = r?;
                let k = k.subslice(prefix.len(), k.len() - prefix.len());
                let k = String::from_utf8(k.to_vec()).map_err(|e| {
                    DatabaseError::SerializationError(format!(
                        "Could not parse string as utf8: {}",
                        e
                    ))
                })?;
                let v = std::str::from_utf8(&v).map_err(|e| {
                    DatabaseError::SerializationError(format!(
                        "Could not parse string as utf8: {}",
                        e
                    ))
                })?;
                let v = serde_json::from_str(&v).map_err(|e| {
                    DatabaseError::SerializationError(format!(
                        "Could not deserialize due to: {}",
                        e
                    ))
                })?;
                Ok((k, v))
            })
            .try_fold::<_, Result<_, DatabaseError>, _, _>(
                || Vec::new(),
                |mut v, pair| {
                    v.push(pair?);
                    Ok(v)
                },
            )
            .try_reduce(
                || Vec::new(),
                |mut v1, v2| {
                    v1.extend(v2);
                    Ok(v1)
                },
            )?;
        Ok(objects)
    })
    .await
    .map_err(|_| DatabaseError::ConcurrencyError)?
}

/// Gets a value from the database
pub async fn get<D: DeserializeOwned + Send + Sync + 'static>(
    tree: Tree,
    key: String,
) -> std::result::Result<D, DatabaseError> {
    spawn_blocking(move || {
        let key = key.as_bytes();
        let val = match tree.get(key) {
            Ok(Some(val)) => val,
            Ok(None) => return Err(DatabaseError::NotFound),
            Err(e) => return Err(DatabaseError::SledError(e)),
        };
        let val = std::str::from_utf8(&val).map_err(|e| {
            DatabaseError::SerializationError(format!("Could not parse string as utf8: {}", e))
        })?;
        let val = serde_json::from_str(val).map_err(|e| {
            DatabaseError::SerializationError(format!("Could not deserialize due to: {}", e))
        })?;
        Ok(val)
    })
    .await
    .map_err(|_| DatabaseError::ConcurrencyError)?
}

/// `insert` will overwrite any possible value in a certain key if it already exists without
/// returning an error. If you want to return an error use `insert_strict`
pub async fn insert<S: Serialize + Send + Sync + 'static>(
    tree: Tree,
    key: String,
    object: S,
) -> std::result::Result<(), DatabaseError> {
    spawn_blocking(move || {
        let key = key.as_bytes();
        let object = serde_json::to_string(&object);
        let object = match object {
            Ok(object) => object,
            Err(e) => {
                return Err(DatabaseError::SerializationError(format!(
                    "Problem serializing: {}",
                    e
                )))
            }
        };
        match tree.insert(key, &object[..]) {
            Ok(_) => Ok(()),
            Err(e) => Err(DatabaseError::SledError(e)),
        }
    })
    .await
    .map_err(|_| DatabaseError::ConcurrencyError)?
}

/// `insert_strict` will return an error if the key already exists, if you want to ignore it
/// already existing use `insert`
pub async fn insert_strict<S: Serialize + Send + Sync + 'static>(
    tree: Tree,
    key: String,
    object: S,
) -> std::result::Result<(), DatabaseError> {
    spawn_blocking(move || {
        let key = key.as_bytes();
        let object = serde_json::to_string(&object);
        let object = match object {
            Ok(object) => object,
            Err(e) => {
                return Err(DatabaseError::SerializationError(format!(
                    "Problem serializing: {}",
                    e
                )))
            }
        };

        match tree.compare_and_swap(key, None as Option<&[u8]>, Some(&object[..])) {
            Ok(Ok(())) => Ok(()),
            Ok(Err(_)) => Err(DatabaseError::InsertFailedKeyExists),
            Err(_) => Err(DatabaseError::ReadOnlyDatabase),
        }
    })
    .await
    .map_err(|_| DatabaseError::ConcurrencyError)?
}

/// Remove an entry from the database, success even if no old value existed
pub async fn remove(tree: Tree, key: String) -> std::result::Result<(), DatabaseError> {
    spawn_blocking(move || {
        let key = key.as_bytes();
        tree.remove(key)?;
        Ok(())
    })
    .await
    .map_err(|_| DatabaseError::ConcurrencyError)?
}
