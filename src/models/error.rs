use warp::reject::Reject;

impl Reject for Error {}

#[derive(Debug, Clone)]
pub enum Error {
    Authentication(String),
    BadRequest(String),
    NotFound(String),
    InvalidArgument(String),
    SerializationError(String),
    DatabaseError(String),
    InternalError(String),
    Unspecified(String),
    UserAlreadyExists(String),
}

impl Error {
    pub fn username_already_registered() -> Self {
        Self::UserAlreadyExists(format!("User already exists"))
    }

    pub fn insert_failed_key_exists() -> Self {
        Self::DatabaseError(format!("Key already exists"))
    }

    pub fn could_not_get_ip() -> Self {
        Self::Unspecified(format!("Could not get the IP from request"))
    }

    pub fn could_not_generate_token() -> Self {
        Self::Unspecified(format!("Could not generate token"))
    }

    pub fn no_password_registered() -> Self {
        Self::NotFound(format!("No password registered"))
    }

    pub fn no_public_key_registered() -> Self {
        Self::NotFound(format!("No public key registered"))
    }

    pub fn opaque_internal_error() -> Self {
        Self::InternalError(format!("Internal error"))
    }

    pub fn failed_authentication() -> Self {
        Self::Authentication(format!("Failed authentication"))
    }

    pub fn user_not_found() -> Self {
        Self::NotFound(format!("Could not find user"))
    }

    pub fn invalid_url_query_string() -> Self {
        Self::InvalidArgument(format!("Could not parse query"))
    }

    pub fn corrupt_token() -> Self {
        Self::InvalidArgument(format!("Could not parse token"))
    }

    pub fn invalid_range_header() -> Self {
        Self::InvalidArgument(format!("Could not parse Range header"))
    }

    pub fn warp_err(err: Error) -> warp::Rejection {
        warp::reject::custom(err)
    }
}
