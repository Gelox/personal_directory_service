use crate::models::PubKey;
use serde::{Deserialize, Serialize};
use std::net::IpAddr;

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SearchResult {
    pub username: String,
    pub ip: IpAddr,
    pub pub_key: Option<PubKey>,
}
