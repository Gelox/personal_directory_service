use crate::models::PubKey;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct User {
    pub username: String,
    pub pub_key: Option<PubKey>,
    pub hashed_password: String,
}
