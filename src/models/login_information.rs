use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::net::IpAddr;

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct LoginInformation {
    pub username: String,
    pub expires: DateTime<Utc>,
    pub ip: IpAddr,
    pub keyword_filters: Option<Vec<String>>,
    pub username_filters: Option<Vec<String>>,
    pub token: String,
}
