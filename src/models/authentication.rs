use serde::Serialize;

/// A signature is made up of a message containing:
/// <service_name>-<date_of_signing>
/// Ex: personal_directory_service-2020-01-01
/// This may cause issues when signing in via this method right at the moment of
/// transitioning from one day to another. This should be handled by clients by either
/// signing with the date that will be transitioned to or by resigning after a
/// rejection.
/// The reason for signing the date is to make sure that signatures become invalid after
/// a day in case of compromise.
/// The reason for signing with service name is to avoid attacks where a user logs in to
/// two different servers via signature and one of the servers uses the signature to log
/// in to the other service as that user.
///
/// This signature should be provided in base64 form
#[derive(Serialize, Debug)]
pub enum Authentication {
    Password(Password),
    Signature(Signature),
}
