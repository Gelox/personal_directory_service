use crate::models::Error;
use log::error;
use std::convert::Infallible;
use warp::http::StatusCode;
use warp::{reply::Reply, Rejection};

pub async fn handle_rejection(err: Rejection) -> Result<impl Reply, Infallible> {
    if let Some(err) = err.find::<Error>() {
        match err {
            Error::BadRequest(s) => {
                error!("Bad request: {}", s);
                Ok(warp::reply::with_status(
                    s.to_string(),
                    StatusCode::BAD_REQUEST,
                ))
            }
            Error::NotFound(s) => {
                error!("Not found: {}", s);
                Ok(warp::reply::with_status(
                    s.to_string(),
                    StatusCode::NOT_FOUND,
                ))
            }
            Error::InvalidArgument(s) => {
                error!("Invalid argument: {}", s);
                Ok(warp::reply::with_status(
                    s.to_string(),
                    StatusCode::BAD_REQUEST,
                ))
            }
            Error::SerializationError(s) => {
                error!("Serialization error: {}", s);
                Ok(warp::reply::with_status(
                    format!("Internal error"),
                    StatusCode::INTERNAL_SERVER_ERROR,
                ))
            }
            Error::Authentication(s) => {
                error!("Authorization error: {}", s);
                Ok(warp::reply::with_status(
                    format!("Unauthorized"),
                    StatusCode::UNAUTHORIZED,
                ))
            }
            Error::InternalError(s) => {
                error!("Internal error: {}", s);
                Ok(warp::reply::with_status(
                    format!("Internal error"),
                    StatusCode::INTERNAL_SERVER_ERROR,
                ))
            }
            Error::Unspecified(s) => {
                error!("Unspecified error: {}", s);
                Ok(warp::reply::with_status(
                    format!("Internal error"),
                    StatusCode::INTERNAL_SERVER_ERROR,
                ))
            }
            Error::DatabaseError(s) => {
                error!("Database error: {}", s);
                Ok(warp::reply::with_status(
                    format!("Internal error"),
                    StatusCode::INTERNAL_SERVER_ERROR,
                ))
            }
            Error::UserAlreadyExists(s) => {
                error!("User already exists {}", s);
                Ok(warp::reply::with_status(
                    format!("User already exists"),
                    StatusCode::CONFLICT,
                ))
            }
        }
    } else if let Some(e) = err.find::<warp::filters::body::BodyDeserializeError>() {
        error!("Body deserialization error : {}", e);
        Ok(warp::reply::with_status(
            format!("Request body incorrectly formatted"),
            StatusCode::BAD_REQUEST,
        ))
    } else {
        error!("Unknown error: {:?}", err);
        Ok(warp::reply::with_status(
            format!("Unknown error"),
            StatusCode::INTERNAL_SERVER_ERROR,
        ))
    }
}
