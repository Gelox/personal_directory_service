use lazy_static::lazy_static;
use sled::Tree;
mod filters;
use crate::filters::filters;
mod tests;
use futures::future::join;

mod api;
mod database;
mod handle_rejection;
mod models;
mod utils;

type Result<T> = std::result::Result<T, models::Error>;

const TOPIC_DELIMITER: char = ';';

// TEST
#[cfg(debug_assertions)]
lazy_static! {
    static ref CERT_PATH: String =
        std::env::var("CERT_PATH").expect("CERT_PATH env variable not set");
    static ref CERT_KEY_PATH: String =
        std::env::var("CERT_KEY_PATH").expect("CERT_KEY_PATH env variable not set");
    static ref SERVICE_NAME: String = String::from("Personal directory service");
    static ref SLED_DB_PATH: String = String::from("my_little_test_db");
}
// PROD
#[cfg(not(debug_assertions))]
lazy_static! {
    static ref CERT_PATH: String =
        std::env::var("CERT_PATH").expect("CERT_PATH env variable not set");
    static ref CERT_KEY_PATH: String =
        std::env::var("CERT_KEY_PATH").expect("CERT_KEY_PATH env variable not set");
    static ref SERVICE_NAME: String = String::from("Personal directory service");
    static ref SLED_DB_PATH: String = String::from("my_little_db");
}

lazy_static! {
    static ref DB: sled::Db = sled::open(&*SLED_DB_PATH).expect("Could not open database");
    static ref USER_TREE: Tree = DB.open_tree("USERS").expect("Could not open user tree");
    static ref LOGIN_TREE: Tree = DB.open_tree("LOGIN").expect("Could not open login tree");
    static ref TOPIC_TREE: Tree = DB.open_tree("TOPICS").expect("Could not open topic tree");
}

#[tokio::main]
async fn main() {
    utils::init().expect("Could not initiate logger");
    let routes = filters();
    let addr = [0, 0, 0, 0];
    if cfg!(debug_assertions) {
        warp::serve(routes).run((addr, 8080)).await;
    } else {
        let https = warp::serve(routes.clone())
            .tls()
            .cert_path(&*CERT_PATH)
            .key_path(&*CERT_KEY_PATH)
            .run((addr, 8443));
        let http = warp::serve(routes).run((addr, 8080));
        join(http, https).await;
    }
}
